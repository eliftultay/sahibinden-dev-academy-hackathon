package com.example.repository;

import com.example.model.entity.Classified;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassifiedRepository extends JpaRepository<Classified, Long> {
}
