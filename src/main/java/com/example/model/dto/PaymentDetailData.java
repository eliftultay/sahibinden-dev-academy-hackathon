package com.example.model.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PaymentDetailData {
    long id;
    long classifiedId;
    String createdDate;
    BigDecimal amount;
    BigDecimal discount;
}
