package com.example.model.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "ACCESSLOG")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccessLog {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @ManyToOne
    @JoinColumn(name="USERID")
    User user;

    @Column(name = "ENDPOINT")
    String endpoint;

    @Column(name = "CREATEDATE")
    String createdDate;

}
