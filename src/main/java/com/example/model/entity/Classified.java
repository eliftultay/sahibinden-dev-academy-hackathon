package com.example.model.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "CLASSIFIED")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Classified {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @ManyToOne
    @JoinColumn(name = "USERID")
    User user;

    @Column(name = "TITLE")
    String title;

    @Column(name = "DESCRIPTION")
    String description;

    @Column(name = "CURRENCY")
    String currency;

    @Column(name = "PRICE")
    BigDecimal price;

    @Column(name = "ACTIVE")
    Boolean isActive;

    @Column(name = "CREATEDDATE")
    String createdDate;

    @Column(name = "PUBLISHEDBY")
    String publishedBy;

    @Column(name = "CITY")
    String city;

    @Column(name = "CATEGORY")
    String category;

}
