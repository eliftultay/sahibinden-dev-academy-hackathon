package com.example.model.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "PAYMENTDETAIL")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PaymentDetail {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @OneToOne
    @JoinColumn(name = "CLASSIFIEDID")
    Classified classified;

    @Column(name = "CREATEDDATE")
    String createdDate;

    @Column(name = "AMOUNT")
    BigDecimal amount;

    @Column(name = "DISCOUNT")
    BigDecimal discount;

}
