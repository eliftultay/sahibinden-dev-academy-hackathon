package com.example.model.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "USER")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @Column(name = "FIRSTNAME")
    String firstName;

    @Column(name = "LASTNAME")
    String lastName;

    @Column(name = "ACTIVE")
    Boolean isActive;

    @OneToMany(mappedBy="user", fetch = FetchType.LAZY)
    Set<AccessLog> accessLogs;

    @OneToMany(mappedBy="user", fetch = FetchType.LAZY)
    Set<Classified> classifieds;
}
