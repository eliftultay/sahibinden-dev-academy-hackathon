package com.example.model.request;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@RequiredArgsConstructor
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class PostOfferRequest {
    long itemId;
    long accountId;
    int offerAmount;
    @DateTimeFormat(pattern = "yyyy.MM.dd HH:mm:ss.SSS")
    @NotNull
    LocalDateTime offerTime;
}
