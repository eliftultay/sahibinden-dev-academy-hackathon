package com.example.model.response;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccessLogResponse {
    long id;
    long userId;
    String endpoint;
    String createdDate;
}
