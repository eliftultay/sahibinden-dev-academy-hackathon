package com.example.model.response;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ClassifiedResponse {
    long id;
    long userId;
    String title;
    String description;
    String currency;
    BigDecimal price;
    String status;
    String createdDate;
    String publishedBy;
    String city;
    String category;
}
