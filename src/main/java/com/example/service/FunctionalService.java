package com.example.service;

import com.example.mapper.CustomMapper;
import com.example.model.entity.User;
import com.example.model.response.CountResponse;
import com.example.model.response.UserResponse;
import com.example.repository.AccessLogRepository;
import com.example.repository.ClassifiedRepository;
import com.example.repository.PaymentDetailRepository;
import com.example.repository.UserRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
public class FunctionalService {

    AccessLogRepository accessLogRepository;
    UserRepository userRepository;
    PaymentDetailRepository paymentDetailRepository;
    ClassifiedRepository classifiedRepository;

    CustomMapper customMapper;

    public UserResponse findUser(long id) {
       User user = userRepository.findById(id).orElseThrow(() -> new RuntimeException());
        return customMapper.map(user);
    }

    public CountResponse getAccessLogCountByClassifiedId(long classifiedId) {
        long count = accessLogRepository.countByEndpoint("/v1/api/classifieds/" + classifiedId);
        CountResponse response = new CountResponse();
        response.setCount(count);
        return response;
    }

}
