package com.example.service;

import com.example.mapper.CustomMapper;
import com.example.model.dto.*;
import com.example.model.entity.AccessLog;
import com.example.model.entity.Classified;
import com.example.model.entity.PaymentDetail;
import com.example.model.entity.User;
import com.example.repository.AccessLogRepository;
import com.example.repository.ClassifiedRepository;
import com.example.repository.PaymentDetailRepository;
import com.example.repository.UserRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Service
@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
public class FetchDataService {

    String URI_USERS = "https://api-devakademi.sahibinden.com/v1/api/users";
    String URI_ACCESS_LOGS = "https://api-devakademi.sahibinden.com/v1/api/access-logs";
    String URI_CLASSIFIEDS = "https://api-devakademi.sahibinden.com/v1/api/classifieds";
    String URI_PAYMENT_DETAILS = "https://api-devakademi.sahibinden.com/v1/api/payment-details";

    @Autowired
    RestTemplate restTemplate;

    AccessLogRepository accessLogRepository;
    UserRepository userRepository;
    PaymentDetailRepository paymentDetailRepository;
    ClassifiedRepository classifiedRepository;

    CustomMapper customMapper;

    @EventListener(ApplicationReadyEvent.class)
    public void fetchDataAfterStartup() {
        getUsers();
        getAccessLogs();
        getClassifieds();
        getPaymentDetails();
    }

    public void getUsers() {
        if(userRepository.count() != 0)
            return;
        List<User> users = new ArrayList<>();

        for (int i = 0; i < 25; i++) {
            ResponseEntity<UserListData> userListData = restTemplate.getForEntity(URI_USERS + "?pageNo=" + i, UserListData.class);
            List<UserData> userList = userListData.getBody().getData();
            for (UserData userData: userList) {
                User user = new User();
                user.setId(userData.getId());
                user.setFirstName(userData.getFirstName());
                user.setLastName(userData.getLastName());
                if(userData.getStatus().equals("ACTIVE")) {
                    user.setIsActive(true);
                } else {
                    user.setIsActive(false);
                }

                users.add(user);
            }
        }
        userRepository.saveAll(users);
    }

    public void getAccessLogs() {
        if(accessLogRepository.count() != 0)
            return;
        List<AccessLog> accessLogs = new ArrayList<>();

        for (int i = 1; i < 25; i++) {
            ResponseEntity<AccessDatalist>  responseEntity = restTemplate.getForEntity(URI_ACCESS_LOGS + "?pageNo=" + i, AccessDatalist.class);
            List<AccessData> accessDataList = responseEntity.getBody().getData();
            for (AccessData accessData: accessDataList) {
                AccessLog accessLog = new AccessLog();
                accessLog.setId(accessData.getId());
                accessLog.setUser(userRepository.findById(accessData.getUserId()).orElse(userRepository.getOne(1L)));
                accessLog.setEndpoint(accessData.getEndpoint());
                accessLog.setCreatedDate(accessData.getCreatedDate());

                accessLogs.add(accessLog);
            }
        }
        accessLogRepository.saveAll(accessLogs);
    }

    public void getClassifieds() {
        if(classifiedRepository.count() != 0)
            return;

        for (int i = 0; i < 25; i++) {
            ResponseEntity<ClassifiedDatalist>  responseEntity = restTemplate.getForEntity(URI_CLASSIFIEDS + "?pageNo=" + i, ClassifiedDatalist.class);
            List<ClassifiedData> classifiedDataList = responseEntity.getBody().getData();
            for (ClassifiedData classifiedData: classifiedDataList) {
                // classifiedAttributes bilgilerinin alınıp kaydedilmesi
                if(classifiedRepository.existsById(classifiedData.getId()))
                    continue;
                Classified classified = customMapper.map(classifiedData);
                classified.setUser(userRepository.findById(classifiedData.getUserId()).orElse(null));
                if(classifiedData.getStatus().equals("ACTIVE")) {
                    classified.setIsActive(true);
                } else {
                    classified.setIsActive(false);
                }

                classifiedRepository.save(classified);
            }
        }
    }

    public void getPaymentDetails() {
        if(paymentDetailRepository.count() != 0)
            return;
        List<PaymentDetail> paymentDetails = new ArrayList<>();

        for (int i = 0; i < 25; i++) {
            ResponseEntity<PaymentDetailDatalist>  responseEntity = restTemplate.getForEntity(URI_PAYMENT_DETAILS + "?pageNo=" + i, PaymentDetailDatalist.class);
            List<PaymentDetailData> paymentDetailDataList = responseEntity.getBody().getData();
            for (PaymentDetailData paymentDetailData: paymentDetailDataList) {
                PaymentDetail paymentDetail = customMapper.map(paymentDetailData);
                paymentDetail.setClassified(classifiedRepository.findById(paymentDetailData.getClassifiedId()).orElse(null));
                paymentDetails.add(paymentDetail);
            }
        }
        paymentDetailRepository.saveAll(paymentDetails);
    }

}
