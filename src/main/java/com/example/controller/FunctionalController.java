package com.example.controller;

import com.example.model.response.CountResponse;
import com.example.model.response.UserResponse;
import com.example.service.FunctionalService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class FunctionalController {

    FunctionalService functionalService;

    @GetMapping(path = "/user/{userId}")
    public ResponseEntity<UserResponse> getAllItems(@PathVariable(required = true) Long userId) {
        UserResponse userResponse = functionalService.findUser(userId);
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    @GetMapping(path = "/access-log")
    public ResponseEntity<CountResponse> getAccessLogCountByClassifiedId(@RequestParam Long classifiedId) {
        CountResponse countResponse = functionalService.getAccessLogCountByClassifiedId(classifiedId);
        return new ResponseEntity<>(countResponse, HttpStatus.OK);
    }

}