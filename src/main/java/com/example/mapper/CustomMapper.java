package com.example.mapper;

import com.example.model.dto.ClassifiedData;
import com.example.model.dto.PaymentDetailData;
import com.example.model.entity.AccessLog;
import com.example.model.entity.Classified;
import com.example.model.entity.PaymentDetail;
import com.example.model.entity.User;
import com.example.model.response.AccessLogResponse;
import com.example.model.response.ClassifiedResponse;
import com.example.model.response.UserResponse;
import org.mapstruct.Mapper;

import java.util.Set;
import java.util.stream.Collectors;

@Mapper
public interface CustomMapper {
    default UserResponse map(User user) {
        UserResponse userResponse = new UserResponse();
        userResponse.setId(user.getId());
        userResponse.setFirstName(user.getFirstName());
        userResponse.setLastName(user.getLastName());
        userResponse.setStatus(user.getIsActive() ? "ACTIVE" : "PASSIVE");
        userResponse.setClassifieds(mapToClassifiedResponseSet(user.getClassifieds()));
        userResponse.setAccessLogs(map(user.getAccessLogs()));
        return userResponse;
    }

    default Classified map(ClassifiedData classifiedData) {
        Classified classified = new Classified();
        classified.setId(classifiedData.getId());
        classified.setTitle(classifiedData.getTitle());
        classified.setDescription(classifiedData.getDescription());
        classified.setCurrency(classifiedData.getCurrency());
        classified.setPrice(classifiedData.getPrice());
        classified.setCreatedDate(classifiedData.getCreatedDate());
        classified.setPublishedBy(classifiedData.getPublishedBy());
        classified.setCity(classifiedData.getCity());
        classified.setCategory(classifiedData.getCategory());
        return classified;
    }

    default PaymentDetail map(PaymentDetailData paymentDetailData) {
        PaymentDetail paymentDetail = new PaymentDetail();
        paymentDetail.setId(paymentDetailData.getId());
        paymentDetail.setCreatedDate(paymentDetailData.getCreatedDate());
        paymentDetail.setAmount(paymentDetailData.getAmount());
        paymentDetail.setDiscount(paymentDetailData.getDiscount());
        return paymentDetail;
    }

    default Set<AccessLogResponse> map(Set<AccessLog> accessLogSet) {
        return accessLogSet.stream().map(this::map).collect(Collectors.toSet());
    }

    default AccessLogResponse map(AccessLog accessLog) {
        AccessLogResponse accessLogResponse = new AccessLogResponse();
        accessLogResponse.setId(accessLog.getId());
        accessLogResponse.setUserId(accessLog.getUser().getId());
        accessLogResponse.setEndpoint(accessLog.getEndpoint());
        accessLogResponse.setCreatedDate(accessLog.getCreatedDate());
        return accessLogResponse;
    }

    default Set<ClassifiedResponse> mapToClassifiedResponseSet(Set<Classified> classifiedSet) {
        return classifiedSet.stream().map(this::map).collect(Collectors.toSet());
    }

    default ClassifiedResponse map(Classified classifiedData) {
        ClassifiedResponse classifiedResponse = new ClassifiedResponse();
        classifiedResponse.setId(classifiedData.getId());
        classifiedResponse.setUserId(classifiedData.getUser().getId());
        classifiedResponse.setTitle(classifiedData.getTitle());
        classifiedResponse.setDescription(classifiedData.getDescription());
        classifiedResponse.setCurrency(classifiedData.getCurrency());
        classifiedResponse.setStatus(classifiedData.getIsActive() ? "ACTIVE" : "PASSIVE");
        classifiedResponse.setPrice(classifiedData.getPrice());
        classifiedResponse.setCreatedDate(classifiedData.getCreatedDate());
        classifiedResponse.setPublishedBy(classifiedData.getPublishedBy());
        classifiedResponse.setCity(classifiedData.getCity());
        classifiedResponse.setCategory(classifiedData.getCategory());
        return classifiedResponse;
    }
}
